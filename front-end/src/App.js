import React from 'react';

import HomeContainer from './home/HomeContainer.js'
import AboutContainer from './about/AboutContainer.js'
import SettingsContainer from './settings/SettingsContainer.js'
import PlotsContainer from './plots/PlotsContainer.js'

import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link
} from "react-router-dom";

import {Hello} from 'SharedModule'

function App() {
	Hello("front-end user")

	return (
		<div className="app">
			<Router>
				<div className="appRouterSideBar">
					<div className="routerSideBarButton">
						<Link to="/" className="routerLink">Home</Link>
					</div>
					<div className="routerSideBarButton">
						<Link to="/about" className="routerLink">About</Link>
					</div>
					<div className="routerSideBarButton">
						<Link to="/plots" className="routerLink">Plots</Link>
					</div>
					<div className="routerSideBarButton">
						<Link to="/settings" className="routerLink">Settings</Link>
					</div>
				</div>

				<div className="appMainPage">
					<Switch>
						<Route path="/about">
							<AboutContainer />
						</Route>
						<Route path="/plots">
							<PlotsContainer />
						</Route>
						<Route path="/settings">
							<SettingsContainer />
						</Route>
						<Route path="/">
							<HomeContainer />
						</Route>
					</Switch>
				</div>
			</Router>
		</div>
	);
}

export default App;
