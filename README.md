# React Express MySQL full-stack example

An example web page based on the following technologies:
- [React](https://reactjs.org/) front-end with functional components
- [Express](http://expressjs.com/) back-end
- [MySQL](https://www.mysql.com/) database
- [Axios](https://github.com/axios/axios) for front-end REST API calls
- [Plotly](https://plotly.com/) for plotting

## Usage

Read the READMEs in front-end and back-end folders

## Warning

There is no user rights management for database access.

## Licence

MIT licence
