CREATE TABLE Test (
	id int unsigned primary key auto_increment,
	test_name varchar(50),
	add_date date,
	success bool,
	value_1 int,
	value_2 decimal(12,6)
);
