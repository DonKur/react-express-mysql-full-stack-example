# Install

## MySQL

	sudo apt install mysql-client

### Start MySQL server

	mysql

### Change root password

	ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'newrootpassword';

### Databse config file

Create 'configuration.json' file in the back-end folder with the following contents:

	{
		"host": "localhost",
		"user": "root",
		"password": "newrootpassword",
		"database": "example_database"
	}

This file is ignored by git.

### Create database with initial data

Run the following queries in mysql (located in the sql_queries folder):
- CreateDatabase
- CreateTables
- InsertTests

## Node

	npm install

# Run

	npm start

# Build

	npm run build
